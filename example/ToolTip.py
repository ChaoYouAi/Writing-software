import tkinter
import publicMethods #公共方法、函数定义模块

"""==================【ToolTip类代码】=============================================          
#由于tkinter中没有ToolTip功能，这里增加一个 ToolTip 类来实现该功能
【使用方法】
先加入 createToolTip 方法：

def createToolTip(widget,text):#创建 ToolTip 即显示组件提示文本
    
    toolTip = ToolTip.ToolTip(widget)#可带 tk 变量作为 parent
    def enter(event):
        toolTip.setTime(1000*8)#默认显示8秒后关闭
        toolTip.showtip(text)

    def leave(event):
        toolTip.hidetip()

    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)

        #



"""
class ToolTip(object):
    def __init__(self, widget , time=8000):#time 指定多久后自动隐藏提示（毫秒）
        self.widget = widget
        self.x = self.y = 0
        self.time=time
        self.tipwindow = None

        #
    def showtip(self , text , widget=None , x=0 , y=0): #显示提示信息
        if self.time==0:
            return #未启用
        
        self.text = text

        if self.tipwindow or not self.text:
            return
        
        if widget==None:
            x =  self.widget.winfo_rootx()
            y =  self.widget.winfo_rooty()
        else:
            x =  widget.winfo_rootx() + x - 20
            y =  widget.winfo_rooty() + y - 20

        self.tipwindow = tw = tkinter.Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        h,w=self.getTextInfo(text) #文本行数及最大行长度

        width=int(w * 6) + 5 # 计算框宽度
        height= h * 12 + 5          # 计算框高度

        tw.wm_geometry("{}x{}+{}+{}".format(width , height , x , y - height))
        tw.wm_attributes("-topmost", 1) #保持一直在顶层

        #下面代码为 ToolTip 的背景色、字体样式，可以根据需要手动修改红色部分代码
        self.tipText = tkinter.Text(tw , background="#ffffe0" , relief=tkinter.SOLID , 
                                    borderwidth=1 , font=("宋体", "9", "normal"))
        self.tipText.pack(ipadx=2 , fill=tkinter.BOTH , expand=True)

        self.insertTipText(text)

        tw.after(self.time,self.hidetip)#自动隐藏

        #
    def hidetip(self): #隐藏提示信息
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()

        #
    def getTextInfo(self,text):#返回 文本行数及最大行长度
        lines=text.splitlines()
        maxLen=0
        for l in lines:
            lineLen= publicMethods.getTextRealLen(l) #计算字符长度（中文算2个字符）
            if lineLen>maxLen:
                maxLen=lineLen
        
        return (len(lines) , maxLen)
    
        #
    def setTime(self , time):#重启隐藏时间
        self.time=time
        
        #
    def insertTipText(self , text):#状态栏文本处理子函数
        if len(text)==0:
            return

        self.tipText.insert("end" , text)


