# -*-coding:utf-8 -*-
'''

软件设置窗口模块 



'''

import tkinter
from tkinter import ttk 
import os 
from PIL import Image ,  ImageTk #定义图片按键用模板【安装：pip install Pillow -i https://pypi.douban.com/simple】
from tkinter import simpledialog #简单对话框
import tkinter.messagebox #系统消息框

#■■■■■■■■■■■■■■■■■■ 调用自定义模块
import ToolTip #部件提示模块
import publicMethods #公共方法、函数定义模块


#■■■■■■■■■■■■■■■■■■声明公共常用
from constant import STR_SELF_PATH , STR_IMAGES_DIR #获取当前软件目录

#■■■■■■■■■■■■■■■■■■仅在本模块中用到的常用 (systemSetup类)

INT_SETUP_WINDOW_WIDTH    =340 #管理对话框宽度
INT_SETUP_WINDOW_HEIGHT   =415 #管理对话框高度
STR_SETUP_WINDOW_TITLE = "系统设置窗口"

STRS_LIST_SETUP_TEXTS = ['选择我就对了！',
                         '春天很快就来了',
                         '今年冬天也太冷了',
                         '今天我吃烧串了',
                         '不行了，我要笑',
                         '选择我就揍你！' ]

STR_COMMAND_NOTE_TIPTIME="设置命令提示显示时间\n0 表示不显示提示"

#■■■■■■■■■■■■■■■■■■定义类内容

class systemSetup(tkinter.Toplevel): #系统设置
    isOK=False # 判断是否按下【确定
    tipTime=8 # 命令提示时间

    #■■■■■■■■■■■■■■■■■■ 初始方法
    def __init__(self , parent):
        tkinter.Toplevel.__init__(self , parent)
        self.parent=parent
        
        self.protocol('WM_DELETE_WINDOW' , self.close)#关闭窗口后的事件
        
        #======================================================【父窗口正上方】
        x=int( parent.winfo_x() + 
              (parent.winfo_width() - INT_SETUP_WINDOW_WIDTH) / 2) #子窗口开始x坐标
        y=int( parent.winfo_y() +
              (parent.winfo_height() - INT_SETUP_WINDOW_HEIGHT) / 2) #子窗口开始y坐标

        #self.geometry('{}x{}+{}+{}'.format(INT_DIALOG_WINDOW_WIDTH , INT_DIALOG_WINDOW_HEIGHT , x , y)) #格式('宽x高+x+y')
        self.geometry('+{}+{}'.format( x , y)) #格式('宽x高+x+y')
        self.resizable(False , False)#不得改变尺寸

        self.title(STR_SETUP_WINDOW_TITLE)

        self.initUI()
        self.iconbitmap(os.path.join(STR_SELF_PATH , STR_IMAGES_DIR , 'setup.ico'))


        #
    def initUI(self):#定义 GUI 界面
        self.int_spbTiptime = tkinter.IntVar()
        self.int_chkSetup1  = tkinter.IntVar()
        self.int_chkSetup2  = tkinter.IntVar()
        self.str_textSetup3 = tkinter.StringVar()
        self.int_chkSetup4  = tkinter.IntVar()

        box=tkinter.Frame(self)
        box.pack(fill=tkinter.BOTH , expand = True , padx = 15 , pady = 10)

        row=tkinter.Frame(box)
        row.pack(side=tkinter.TOP)

        tkinter.Label(row,text="命令提示时间 ").pack(side=tkinter.LEFT)
        spbTipTime=tkinter.Spinbox(row , from_=0, to=300, increment=1, width=4 , textvariable=self.int_spbTiptime)
        spbTipTime.pack(side=tkinter.LEFT)
        tkinter.Label(row,text="秒").pack(side=tkinter.LEFT)

        self.createToolTip(spbTipTime,STR_COMMAND_NOTE_TIPTIME)

        row=tkinter.Frame(box)
        row.pack(side=tkinter.TOP)

        chkSetup1=tkinter.Checkbutton(row , text="我的设置1" , variable=self.int_chkSetup1)
        chkSetup1.pack(side=tkinter.LEFT)
        chkSetup2=tkinter.Checkbutton(row , text="我的设置2" , variable=self.int_chkSetup2)
        chkSetup2.pack(side=tkinter.LEFT)


        row=tkinter.Frame(box)
        row.pack(side=tkinter.TOP,fill=tkinter.X)

        tkinter.Label(row,text="设置3").pack(side=tkinter.LEFT)
        textSetup3=tkinter.Entry(row , textvariable=self.str_textSetup3 , width=10)
        textSetup3.pack(side=tkinter.LEFT , fill=tkinter.X , expand=True)

        select=tkinter.LabelFrame(box , text="请选择一种方式")
        select.pack(side=tkinter.TOP , fill=tkinter.X)

        for i in range(len(STRS_LIST_SETUP_TEXTS)): # 循环显示单选设置
            tkinter.Radiobutton(select , text=STRS_LIST_SETUP_TEXTS[i] , value=i , 
                                variable=self.int_chkSetup4 , anchor=tkinter.W
                                ).pack(side=tkinter.TOP , fill=tkinter.X)

        commands=tkinter.Frame(box)
        commands.pack(side=tkinter.TOP)
        
        ctrl_butOK=tkinter.Button(commands , text="确定" , width=8 , command=self.OK)
        ctrl_butOK.pack(side=tkinter.LEFT , padx=15 , pady=6)
        
        ctrl_butClose=tkinter.Button(commands , text="取消", width=8 , command=self.close)
        ctrl_butClose.pack(side=tkinter.LEFT , padx=0 , pady=6)

        
        #
    def createToolTip(self , widget , text):#创建 ToolTip 即显示组件提示文本
        toolTip = ToolTip.ToolTip(widget )#
        def enter(event):
            toolTip.setTime(self.tipTime * 1000)
            toolTip.showtip(text)

        def leave(event):
            toolTip.hidetip()

        widget.bind('<Enter>' ,  enter)
        widget.bind('<Leave>' ,  leave)

        #
    def initUI_datas(self):#初始化界面数据

        #从数据库中取设置

        self.tipTime=int(publicMethods.getDatabSetup('tiptime',default=8))  #取提示时间设置
        self.int_spbTiptime.set(self.tipTime)

        self.int_chkSetup1.set(int(publicMethods.getDatabSetup('setup1',default=1)))
        self.int_chkSetup2.set(int(publicMethods.getDatabSetup('setup2',default=0)))
        self.str_textSetup3.set(publicMethods.getDatabSetup('setup3',default=''))
        self.int_chkSetup4.set(int(publicMethods.getDatabSetup('select',default=0)))

        #
    #■■■■■■■■■■■■■■■■■■ 基本方法
    def show(self):#显示窗体

        self.initUI_datas()#初始化界面数据
        #self.wait_window()
        self.grab_set() #模块化窗口
        self.mainloop()

        #
    #■■■■■■■■■■■■■■■■■■ 按键操作
    def close(self):#关闭窗口
        if self.isOK==False:
            al=tkinter.messagebox.askyesnocancel('放弃改动' , '是否确定放弃本次改动关闭窗口？',parent=self)
            self.grab_set() #防止变为非模式窗口

            if al != True:
                return
            
        self.destroy()

        #
    def OK(self):#确定


        setups=[self.int_spbTiptime.get(),
                self.int_chkSetup1.get(),
                self.int_chkSetup2.get(),
                self.str_textSetup3.get(),
                self.int_chkSetup4.get()]
        
        names=['tiptime',
               'setup1',
               'setup2',
               'setup3',
               'select']
        
        for i in range(len(names)):
            publicMethods.updateDbSetup(names[i],setups[i])  #将设置保存到数据库
        
        self.isOK=True
        self.parent.setTimptime(self.int_spbTiptime.get()) # 改变主窗体命令提示时间
        self.close()

        #




if __name__ == "__main__":
    r=tkinter.Tk()
    #m=systemSetup('')
    #m=WebSiteManager('')
    #m=chapterIndexFormat('')
    r.geometry("+400+400")
    r.update()
    m=systemSetup(r)
    m.show()
    r.mainloop()