# -*-coding:utf-8 -*-
'''
本模块为常量定义模块
统一放在此外方便定位修改

【建议使用方法】===============================================================
在调用模块前加入下列语句
from constant import 常量名称

如：from constant import STR_SELF_PATH,STR_SOFTWARE_NAME,STR_VERSION_NUMBER
   表示要在该模块中使用 【当前软件目录】、【软件名称】、【版本号】 等常量
'''
import os,sys

STR_SELF_PATH=str(os.path.dirname(os.path.realpath(sys.argv[0])))   #获取当前软件目录
if len(STR_SELF_PATH)==0:
    STR_SELF_PATH=os.path.join(os.getcwd()) #第一种方式获取不到目录时


STR_SOFTWARE_NAME        ='软件名称'          #软件名称
STR_VERSION_NUMBER       ='V1.0.0'           #版本号
STR_PUBLICATION_DATE     ='2023年12月22日'   #最后修改时间
STR_IMAGES_DIR           ='images'          #图标目录
SRE_CONVER_CODE          ='utf-8'           #默认的编码'utf-8' 带BOM



#■■■■■■■■■■■■■■■■■■ 数据库定义

STR_SETUP_DB             = 'system.db'      #系统数据库文件名

#===============系统设置表
STR_SETUP_TABLE         ="setup"                #系统设置表

STR_CREATE_DB_TABLE_SETUP="""CREATE TABLE IF NOT EXISTS {} (
            id Integer PRIMARY KEY AUTOINCREMENT,
            name Text , 
            value Text );""".format(STR_SETUP_TABLE) #如果不存在设置表则创建

"""
系统设置表 name 关键字定义表

    name            value[默认值]   说明

    welcome        1            显示迎接屏
    
    ''''

    下面是本例的示范定义
    tiptime        8            命令提示时间
    setup1         1            设置1
    setup2         0            设置1
    setup3         我的软件      设置3
    select         0            我的选择

    name 可根据设置自定义，可以是数值型或字符型，数据一均按字符型来保存，数值型在使用时注意转换格式
    """