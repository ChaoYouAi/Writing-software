#■■■■■■■■■■■■■■■■■■ 调用模块
# -*-coding:utf-8 -*-
"""
关于窗口模块

"""

#■■■■■■■■■■■■■■■■■■ 调用模块
import tkinter


#■■■■■■■■■■■■■■■■■■ 调用自定义模块

import publicMethods  #公共方法、函数定义模块

#■■■■■■■■■■■■■■■■■■声明公共常用

from constant import STR_SOFTWARE_NAME #软件名称
from constant import STR_VERSION_NUMBER #工具版本号
from constant import STR_PUBLICATION_DATE #最后修改时间

#■■■■■■■■■■■■■■■■■■仅在本模块中用到的常用
INT_ABOUT_WINDOW_WIDTH        = 350 #默认窗体宽度
INT_ABOUT_WINDOW_HEIGHT       = 300 #默认窗体高度
STR_ABOUT_WINDOW_TITLE        = "关于..."
STR_ABOUT_NOTE                ="""    我的软件是最好用的！

    这个软件集合上万种功能，无所不能，上天下地，为我独尊……
"""

class about(tkinter.Toplevel):
    
#---初始化函数--------------------------------
    def __init__(self , parent):#类初始化
        tkinter.Toplevel.__init__(self,parent)
        self.transient(parent)

        x = int((self.winfo_screenwidth()-INT_ABOUT_WINDOW_WIDTH)/2) #子窗口开始x坐标
        y = int((self.winfo_screenheight()-INT_ABOUT_WINDOW_HEIGHT)/2) #子窗口开始y坐标

        self.geometry('{}x{}+{}+{}'.format(INT_ABOUT_WINDOW_WIDTH , INT_ABOUT_WINDOW_HEIGHT , x , y ))
        self.title(STR_ABOUT_WINDOW_TITLE)
        self.resizable(False,False)#不得改变尺寸


        self.loadImages()
        self.initUI()#界面结构

        
        #
    def initUI(self):#界面结构
        
        box=tkinter.Frame(self)
        box.pack(fill=tkinter.BOTH , expand = True , padx = 15 , pady = 10)

        row1=tkinter.Frame(box)
        row1.pack(side=tkinter.TOP , fill=tkinter.X , expand=True)


        imgIcon=tkinter.Label(row1 , image=self.imgIcon)#注意图片尺寸，建议图片分辨率为:88*84
        imgIcon.pack(side=tkinter.LEFT)

        infos=tkinter.Frame(row1)
        infos.pack(side=tkinter.LEFT , fill=tkinter.X , expand=True)

        tkinter.Label(infos, text=STR_SOFTWARE_NAME , 
                      font=('宋体', '14','bold')).pack(side=tkinter.TOP , fill=tkinter.X ,
                                                     expand=True , anchor=tkinter.CENTER ) # 软件名称

        tkinter.Label(infos, text='版本号：{}'.format(STR_VERSION_NUMBER) , fg='#C000C0',
                      font=('宋体', '11')).pack(side=tkinter.TOP)  # 版本号

        tkinter.Label(infos, text=STR_PUBLICATION_DATE , 
                      font=('宋体', '9')).pack(side=tkinter.TOP , anchor=tkinter.SE) # 更新日期

        row2=tkinter.Frame(box)
        row2.pack(side=tkinter.TOP , fill=tkinter.BOTH , expand=True)

        textNote=publicMethods.createScrollbarWidget(row2 , type = 1 , height =10 )
        textNote.insert('end' , STR_ABOUT_NOTE)
        textNote.config(state=tkinter.DISABLED)  # 不可编辑

        row3=tkinter.Frame(box)
        row3.pack(side=tkinter.TOP,fill=tkinter.X , expand=True)

        butClose=tkinter.Button(row3, text='关闭' , width = 10 , command=self.close)#可在括号内加上调用函数部分 
        butClose.pack()


        #
    def loadImages(self):#预先加载图标
        self.imgIcon = publicMethods.loadImage("icon.ico")
        
        #
    def show(self):#显示窗体
    
        self.grab_set() #模块化窗口
        self.mainloop()
        #self.wait_window()

        #
    def close(self):# 关闭窗口
        self.destroy()

        #


#---调试时用
if __name__ == "__main__":
    ab=about(None)
    ab.show()#显示窗体，不创建任务
#tts=about(None)
#tts.showWindow()#显示窗体，不创建任务